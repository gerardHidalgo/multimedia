package com.example.gerar.practicamultimedia;


import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

public class Audio extends AppCompatActivity {

    private MediaRecorder gravador;
    private MediaPlayer reproductor;

    private File fitxer;

    private ImageView recordButton, playButton, stopRecord, stopPlay, imageAudioView;


    public Audio(){
        super();
        String fileNameStr = "demoaudio.3gp";
        fitxer = new File(Environment.getExternalStorageDirectory(), "Mohinux");
        if(!fitxer.exists()){
            fitxer.mkdirs();
        }

        fitxer = new File(fitxer, fileNameStr);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        configGUI();

        imageAudioView.setImageResource(R.drawable.mic_tencat);

        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRecording();
                imageAudioView.setImageResource(R.drawable.mic_obert);
            }
        });

        stopRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopRecording();
                imageAudioView.setImageResource(R.drawable.mic_tencat);
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPlaying();
                imageAudioView.setImageResource(R.drawable.start_audio);
            }
        });

        stopPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopPlaying();
                imageAudioView.setImageResource(R.drawable.stop_audio);
            }
        });

    }

    private void configGUI(){

        recordButton = (ImageView) findViewById(R.id.recordButton);
        playButton = (ImageView) findViewById(R.id.playButton);
        stopRecord = (ImageView) findViewById(R.id.stopRecordButton);
        stopPlay = (ImageView) findViewById(R.id.stopPlayButton);
        imageAudioView = (ImageView) findViewById(R.id.img_audioview);
    }

    private void stopRecording() {
        gravador.stop();
        gravador.release();
        gravador = null;
    }

    private void startRecording() {
        crearGravador();
    }

    private void stopPlaying() {
        reproductor.stop();
        reproductor.release();
        reproductor = null;
    }

    private void startPlaying() {
        reproductor  =new MediaPlayer();
        try {
            reproductor.setDataSource(fitxer.getAbsolutePath());
            reproductor.prepare();
            reproductor.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void crearGravador(){
        gravador = new MediaRecorder();
        gravador.setAudioSource(MediaRecorder.AudioSource.MIC);
        gravador.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        gravador.setOutputFile(fitxer.getPath());
        gravador.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            gravador.prepare();
            gravador.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}