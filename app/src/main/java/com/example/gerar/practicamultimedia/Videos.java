package com.example.gerar.practicamultimedia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class Videos extends AppCompatActivity {

    private VideoView videoView;
    private MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);

        videoView = (VideoView) findViewById(R.id.videoView);
        mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        videoView.setVideoPath("https://firebasestorage.googleapis.com/v0/b/multimedia-693bb.appspot.com/o/OP-Ep.830.mp4?alt=media&token=07ae6ba1-6f14-4b4e-b7cc-fc792fa6ce5e");
        videoView.start();

    }
}
