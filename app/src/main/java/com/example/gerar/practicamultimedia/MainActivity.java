package com.example.gerar.practicamultimedia;

import android.*;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.gerar.practicamultimedia.Login;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout opInfo, opAudio, opVideo, opGVid;
    private MediaPlayer playerAudio = new MediaPlayer();
    private ImageView audioController, userPhoto;
    private Editable userName;
    private Editable userLastName;
    private String usuari;
    private TextView userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userName = Login.nom.getText();
        userLastName = Login.cognom.getText();
        usuari = userName + " " + userLastName;
        userId = (TextView) findViewById(R.id.UserID);

        if(usuari.equals(" "))
            userId.setText("Monkey D. Luffy");
        else
            userId.setText(usuari);


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.RECORD_AUDIO}, 0);

        try{
            AssetFileDescriptor fitxerAudio = getAssets().openFd("opa.mp3");
            playerAudio.setDataSource(fitxerAudio.getFileDescriptor(),fitxerAudio.getStartOffset(),fitxerAudio.getLength());
            fitxerAudio.close();

            playerAudio.prepare();
            playerAudio.setLooping(true);
            playerAudio.start();


        } catch (Exception ex){
            ex.printStackTrace();
        }

        configGUI();

        audioController.setImageResource(R.drawable.logo_audio);

        audioController.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(playerAudio.isPlaying()){
                    playerAudio.pause();
                    audioController.setImageResource(R.drawable.logo_no_audio);
                }else{
                    playerAudio.start();
                    audioController.setImageResource(R.drawable.logo_audio);
                }
            }
        });

        try {
            Intents();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void configGUI(){

        audioController = (ImageView)findViewById(R.id.AudioIcon);
        opInfo = (RelativeLayout)findViewById(R.id.opcioInfo);
        opVideo = (RelativeLayout)findViewById(R.id.opcioVideos);
        opAudio = (RelativeLayout)findViewById(R.id.opcioAudio);
        opGVid = (RelativeLayout)findViewById(R.id.opcioGVid);
        userPhoto = (ImageView)findViewById(R.id.UserImage);

    }

    private void Intents() throws IOException {
        opInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent info = new Intent(view.getContext(), Info.class);
                if(playerAudio.isPlaying()){
                    playerAudio.pause();
                    audioController.setImageResource(R.drawable.logo_no_audio);
                }
                startActivity(info);
            }
        });

        opVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent video = new Intent(view.getContext(), Videos.class);
                if(playerAudio.isPlaying()){
                    playerAudio.pause();
                    audioController.setImageResource(R.drawable.logo_no_audio);
                }
                startActivity(video);
            }
        });

        opAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent audio = new Intent(view.getContext(), Audio.class);
                if(playerAudio.isPlaying()){
                    playerAudio.pause();
                    audioController.setImageResource(R.drawable.logo_no_audio);
                }
                startActivity(audio);
            }
        });

        opGVid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent foto = new Intent(view.getContext(), GravarVideos.class);
                if(playerAudio.isPlaying()){
                    playerAudio.stop();
                    audioController.setImageResource(R.drawable.logo_no_audio);
                }
                startActivity(foto);
            }
        });

        if(Login.ruta!=null){
            userPhoto.setImageBitmap(MediaStore.Images.Media.getBitmap(
                    getContentResolver(),
                    Uri.fromFile(Login.ruta)));
        }



    }
}