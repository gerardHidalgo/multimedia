package com.example.gerar.practicamultimedia;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.provider.MediaStore.Images.Media;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Login extends AppCompatActivity {

    static final int CAMERA_APP_CODE = 100; // per exemple...
    ImageView mImageView;
    Button entrar;
    protected File tempImageFile;
    static EditText nom;
    static EditText cognom;
    static File ruta=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mImageView = (ImageView) findViewById(R.id.imageView);



        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        nom = (EditText) findViewById(R.id.editNom);
        cognom = (EditText) findViewById(R.id.editCognoms);

        entrar = findViewById(R.id.entrar);

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }


    protected File crearFitxer() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "foto" + timeStamp + ".jpg";
        File path = new File(Environment.getExternalStorageDirectory(),
                this.getPackageName());
        if (!path.exists()) {
            path.mkdirs();
        }
        return new File(path, imageFileName);
    }

    public void ferUnaFoto(View view) throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(takePictureIntent.resolveActivity(getPackageManager()) != null) {
            tempImageFile = crearFitxer();
            ruta = tempImageFile;
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(tempImageFile));
            startActivityForResult(takePictureIntent, CAMERA_APP_CODE);
        } else {
            Toast.makeText(this, "No hi ha cap aplicació per capturar fotos",
                    Toast.LENGTH_LONG).show();
        }
    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_APP_CODE && resultCode == RESULT_OK) {
            try {
                mImageView.setImageBitmap(Media.getBitmap(
                        getContentResolver(),
                        Uri.fromFile(tempImageFile)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
